import http from 'k6/http';

export default function () {
  http.get(`${__ENV.SERVICE_PROTOCOL}://${__ENV.SERVICE_ENDPOINT}`);
}
